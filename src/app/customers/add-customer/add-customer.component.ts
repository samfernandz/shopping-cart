import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomerService } from '../../core/customer.service';
import { generateCustomerId } from '../../core/util';
import { Customer } from '../models/client/customer';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss']
})
export class AddCustomerComponent implements OnInit {

  nameCtrl: FormControl;
  emailCtrl: FormControl;
  contactNumberCtrl: FormControl;
  addressCtrl: FormControl;
  customerForm: FormGroup;

  lastCustomer: any;

  constructor(
    fb: FormBuilder,
    private customerService: CustomerService
  ) {
    this.nameCtrl = fb.control('', Validators.required);
    this.emailCtrl = fb.control('', [Validators.required, Validators.email]);
    this.contactNumberCtrl = fb.control('', Validators.required);
    this.addressCtrl = fb.control('', Validators.required);
    this.customerForm = fb.group({
      name: this.nameCtrl,
      email: this.emailCtrl,
      contactNumber: this.contactNumberCtrl,
      address: this.addressCtrl,
    });
  }

  ngOnInit() {
    // used this to generate customer item id
    this.getLastCustomer();
  }

  getLastCustomer() {
    this.customerService.getLastCustomer().subscribe(
      customer => {
        this.lastCustomer = customer;
      }
    );
  }

  reset() {
    this.nameCtrl.setValue('');
    this.emailCtrl.setValue('');
    this.contactNumberCtrl.setValue('');
    this.addressCtrl.setValue('');
  }

  async addCustomer() {

    const newCustomer: Customer = {
      ...this.customerForm.value,
      customerId: generateCustomerId(this.lastCustomer.customer_id)
    }

    try {
      const createdCustomer = await this.customerService.createCustomer(newCustomer);
      if (createdCustomer) {
        alert('Item created successfully');
        this.reset();
      }
    } catch (error) {
      console.error(error);
    }
  }





}
