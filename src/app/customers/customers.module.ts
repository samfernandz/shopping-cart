import { NgModule } from '@angular/core';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../shared/shared.module';

const CUSTOMER_ROUTS = [
  {
    path: 'customers/new',
    component: AddCustomerComponent
  }
]


@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(CUSTOMER_ROUTS)
  ],
  declarations: [AddCustomerComponent]
})
export class CustomersModule { }
