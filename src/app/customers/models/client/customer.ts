export interface Customer {
  customerId;
  name;
  email;
  contactNumber;
  address;
}
