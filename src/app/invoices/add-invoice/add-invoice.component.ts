import { Component } from '@angular/core';
import { OrderService } from '../../core/order.service';

@Component({
  selector: 'app-add-invoice',
  templateUrl: './add-invoice.component.html',
  styleUrls: ['./add-invoice.component.scss']
})
export class AddInvoiceComponent {

  selectedInvoice: any;
  isLoading = false;
  isUploadSuccess = false;

  uploadedInvoice: any;

  constructor(private orderService: OrderService) {}

  onFileChanged(event) {
    this.isUploadSuccess = false;
    this.selectedInvoice = event.target.files[0];
  }

  async onUpload() {
    this.isLoading = true;

    const uploadData = new FormData();
    uploadData.append('invoice', this.selectedInvoice, this.selectedInvoice.name);

    try {
      const invoice: any = await this.orderService.uploadInvoice(uploadData);
      if (invoice._id) {
        this.uploadedInvoice = invoice;
        this.isLoading = false;
        this.isUploadSuccess = true;
      }
    } catch (error) {
      console.error(error);
      this.isLoading = false;
      this.isUploadSuccess = false;
    }
  }
}
