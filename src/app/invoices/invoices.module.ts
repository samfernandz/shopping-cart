import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddInvoiceComponent } from './add-invoice/add-invoice.component';
import {AddCustomerComponent} from '../customers/add-customer/add-customer.component';
import {RouterModule} from '@angular/router';
import { ListInvoicesComponent } from './list-invoices/list-invoices.component';
import {SharedModule} from '../shared/shared.module';

const INVOICE_ROUTS = [
  {
    path: 'invoices/new',
    component: AddInvoiceComponent
  },
  {
    path: 'invoices/list',
    component: ListInvoicesComponent
  },
]

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(INVOICE_ROUTS)
  ],
  declarations: [AddInvoiceComponent, ListInvoicesComponent]
})
export class InvoicesModule { }
