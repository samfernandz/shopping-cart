import { NgModule } from '@angular/core';
import { AddLineItemComponent } from './add-line-item/add-line-item.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../shared/shared.module';

const ITEMS_ROUTS = [
  {
    path: 'items/new',
    component: AddLineItemComponent
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ITEMS_ROUTS)
  ],
  declarations: [AddLineItemComponent]
})
export class ItemsModule { }
