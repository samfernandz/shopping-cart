export interface Item {
  itemId;
  name;
  unitPrice;
  quantity;
  description;
}
