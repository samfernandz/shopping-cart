export interface ItemMdl1 {
  _id: string;
  item_id: string;
  name: string;
  unit_price: number;
  quantity: number;
  description: string;
}
