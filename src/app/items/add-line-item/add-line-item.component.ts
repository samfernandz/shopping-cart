import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { generateItemId } from '../../core/util';
import { ItemService } from '../../core/item.service';
import { Item } from '../models/client/item';

@Component({
  selector: 'app-add-line-item',
  templateUrl: './add-line-item.component.html',
  styleUrls: ['./add-line-item.component.scss']
})
export class AddLineItemComponent implements OnInit {

  nameCtrl: FormControl;
  unitPriceCtrl: FormControl;
  quantityCtrl: FormControl;
  descriptionCtrl: FormControl;
  lineItemForm: FormGroup;

  lastItem: any;

  constructor(
    fb: FormBuilder,
    private itemService: ItemService
  ) {
    this.nameCtrl = fb.control('', Validators.required);
    this.unitPriceCtrl = fb.control('', Validators.required);
    this.quantityCtrl = fb.control('', Validators.required);
    this.descriptionCtrl = fb.control('', Validators.required);
    this.lineItemForm = fb.group({
      name: this.nameCtrl,
      unitPrice: this.unitPriceCtrl,
      quantity: this.quantityCtrl,
      description: this.descriptionCtrl,
    });
  }

  ngOnInit() {
    // used this to generate customer item id
    this.getLastCustomer();
  }

  getLastCustomer() {
    this.itemService.getLastItem().subscribe(
      item => {
        this.lastItem = item;
      }
    );
  }

  reset() {
    this.nameCtrl.setValue('');
    this.unitPriceCtrl.setValue('');
    this.quantityCtrl.setValue('');
    this.descriptionCtrl.setValue('');
  }

  async addItem() {

    const newItem: Item = {
      ...this.lineItemForm.value,
      itemId: generateItemId(this.lastItem.item_id)
    }

    try {
      const createdItem = await this.itemService.createItem(newItem);
      if (createdItem) {
        alert('Item created successfully');
        this.reset();
      }
    } catch (error) {
      console.error(error);
    }
  }

}
