
export function generateCustomerId(currentId: string) {
  return `CUS${+currentId.slice(3) + 1}`;
}

export function generateItemId(currentId: string) {
  return `ITM${+currentId.slice(3) + 1}`;
}

export function generateOrderId(currentId: string) {
  return `ORD${+currentId.slice(3) + 1}`;
}



