import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { OrderService } from './order.service';
import {ItemMdl1} from '../items/models/server/item-mdl1';

@Injectable({
  providedIn: 'root'
})
export class ItemResolverService implements Resolve<ItemMdl1[]> {

  constructor(private orderService: OrderService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ItemMdl1[]> {
    return this.orderService.getItems();
  }
}
