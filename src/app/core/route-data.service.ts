import { Injectable } from '@angular/core';
import {OrderItem} from '../orders/model/client/order-item';

@Injectable({
  providedIn: 'root'
})
export class RouteDataService {

  orderItemList: Array<OrderItem>;
  grandTotal: number;

  constructor() { }

}
