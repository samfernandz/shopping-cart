import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true
};

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(private http: HttpClient) { }

  getLastItem() {
    return this.http.get(
      '/api/items/last',
      httpOptions
    );
  }

  createItem(body) {
    return this.http.post(
      '/api/items',
      JSON.stringify(body),
      httpOptions
    ).toPromise();
  }
}
