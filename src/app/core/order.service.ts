import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ItemMdl1 } from '../items/models/server/item-mdl1';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true
};

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) {

  }

  getItems(): Observable<ItemMdl1[]> {
    return this.http.get<ItemMdl1[]>('/api/items');
  }

  getLastOrder() {
    return this.http.get('/api/orders/last');
  }

  createOrderItems(body) {
    return this.http.post(
      '/api/order-items',
      JSON.stringify(body),
      httpOptions
    ).toPromise();
  }

  createOrder(body) {
    return this.http.post(
      '/api/orders',
      JSON.stringify(body),
      httpOptions
    ).toPromise();
  }

  uploadInvoice(formData) {
    return this.http.post('/api/invoices', formData)
      .toPromise();
  }
}
