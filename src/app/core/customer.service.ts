import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CustomerMdl1} from '../customers/models/server/customer-mdl1';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true
};

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient) {}

  // with generic types
  getCustomer(customerId: string): Observable<Array<CustomerMdl1>> {
    return this.http.get<Array<CustomerMdl1>>(`/api/customers/find/${customerId}`);
  }

  getLastCustomer() {
    return this.http.get(
      '/api/customers/last',
      httpOptions
    );
  }

  createCustomer(body) {
    return this.http.post(
      '/api/customers',
      JSON.stringify(body),
      httpOptions
    ).toPromise();
  }
}
