import { Component, OnDestroy, OnInit } from '@angular/core';
import { RouteDataService } from '../../core/route-data.service';
import { OrderItem } from '../model/client/order-item';
import { OrderService } from '../../core/order.service';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, mergeMap } from 'rxjs/operators';
import { CustomerService } from '../../core/customer.service';
import { CustomerMdl1 } from '../../customers/models/server/customer-mdl1';
import {Router} from '@angular/router';
import {generateOrderId} from '../../core/util';

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.scss']
})
export class PlaceOrderComponent implements OnInit, OnDestroy {

  constructor(
      private routeDataService: RouteDataService,
      private orderService: OrderService,
      private customerService: CustomerService,
      private router: Router
  ) { }

  orderItemList: Array<OrderItem>;
  grandTotal: number;

  lastOrder: any;
  customerIdChanged = new Subject<string>();

  customerSubscription;
  listOfGivenCustomers: Array<CustomerMdl1>;
  selectedCustomer: CustomerMdl1;

  isProcessing = false;
  showCustomerList = false;

  shippingAddress: string;

  ngOnInit() {

    // used two fields to generate the table
    this.orderItemList = this.routeDataService.orderItemList;
    this.grandTotal = this.routeDataService.grandTotal;

    // used this to generate order item id
    this.orderService.getLastOrder().subscribe(order => {
      this.lastOrder = order;
    });

    // used this to find the customer based on input
    this.customerSubscription = this.customerIdChanged.pipe(
       debounceTime(500),
       distinctUntilChanged(),
       mergeMap(key => this.getCustomer(key))
    ).subscribe(customers => {
      this.isProcessing = false;
      this.showCustomerList = true;
      this.listOfGivenCustomers = customers;
    });
  }

  searchCustomer(event) {
    this.isProcessing = true;
    this.showCustomerList = false;
    this.customerIdChanged.next(event.target.value);
  }

  getCustomer(key: string) {
    if (!key) {
      this.isProcessing = false;
      return;
    }
    return this.customerService.getCustomer(key);
  }

  selectCustomer(customer: CustomerMdl1) {
    this.selectedCustomer = customer;
    this.showCustomerList = false;
  }

   async placeOrder() {

    const orderItems = [];

    this.orderItemList.forEach(item => {
      orderItems.push({
        itemId: item.itemId,
        name: item.name,
        unitPrice: item.unitPrice,
        quantity: item.quantity,
        total: item.total
      });
    });

    const createdOrderItems = await this.createOrderItems(orderItems);
    const createdOrder = await this.createOrder(createdOrderItems);

    console.log('done', createdOrder );
    this.router.navigate(['/orders/success']);
  }

  ngOnDestroy() {
    this.customerSubscription.unsubscribe();
  }

  async createOrderItems(orderItems) {
    const pArray = orderItems.map( async orderItem => {
      const createdOrderItem = await this.orderService.createOrderItems(orderItem);
      return createdOrderItem;
    });
    const createdOrderItems = await Promise.all(pArray);
    return createdOrderItems;
  }

  async createOrder(createdOrderItems) {

    const orderToBeCreated = {
      orderId: generateOrderId(this.lastOrder.order_id),
      orderItems: createdOrderItems.map(orderItem => orderItem._id),
      customerId: this.selectedCustomer._id,
      shippingAddress: this.shippingAddress,
      grandTotal: this.grandTotal,
      invoice: 'Invoice path'
    }

    return await this.orderService.createOrder(orderToBeCreated);
  }


}
