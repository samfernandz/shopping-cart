import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderItem } from '../model/client/order-item';
import { RouteDataService } from '../../core/route-data.service';
import { ListOrderItemsComponent } from '../list-order-items/list-order-items.component';
import { ItemMdl1 } from '../../items/models/server/item-mdl1';

@Component({
  selector: 'app-add-order-item',
  templateUrl: './add-order-item.component.html',
  styleUrls: ['./add-order-item.component.scss']
})
export class AddOrderItemComponent implements OnInit {

  itemList: Array<ItemMdl1>;
  orderItemList: Array<OrderItem> = [];
  _selectedItem: ItemMdl1;

  orderQuantity = 1;
  itemTotal: number;

  @ViewChild(ListOrderItemsComponent) orderItems: ListOrderItemsComponent;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private routeDataService: RouteDataService
  ) {}

  ngOnInit() {
    if (this.routeDataService.orderItemList) {
      this.orderItemList = this.routeDataService.orderItemList;
    }
    this.itemList = this.route.snapshot.data['items'];
  }

  addItem() {
    const newOrderItem: OrderItem = {
      itemId: this.selectedItem.item_id,
      name: this.selectedItem.name,
      quantity: this.orderQuantity,
      unitPrice: this.selectedItem.unit_price,
      total: this.itemTotal
    };
    this.orderItemList.push(newOrderItem);
  }

  get selectedItem(): ItemMdl1 {
    return this._selectedItem;
  }

  set selectedItem(item: ItemMdl1) {
    this._selectedItem = item;
    this.itemTotal = item.unit_price * this.orderQuantity;
  }

  onChangeQuantity(qty: number) {
    this.orderQuantity = qty;
    this.itemTotal = this.selectedItem.unit_price * qty;
  }

  proceedToPlaceOrder() {
    this.routeDataService.orderItemList = this.orderItemList;
    this.routeDataService.grandTotal = this.orderItems.subTotal;
    this.router.navigate(['/orders/confirm']);
  }


}
