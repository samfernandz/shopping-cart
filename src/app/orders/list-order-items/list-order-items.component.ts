import {Component, Input, OnInit} from '@angular/core';
import {OrderItem} from '../model/client/order-item';

@Component({
  selector: 'app-list-order-items',
  templateUrl: './list-order-items.component.html',
  styleUrls: ['./list-order-items.component.scss']
})
export class ListOrderItemsComponent implements OnInit {

  @Input('orderItemList') orderItemList: Array<OrderItem>;
  isModalActive = false;
  itemIndexToBeDeleted: number;

  constructor() {}

  ngOnInit() {
  }

  onClickDelete(itemIndex) {
    this.itemIndexToBeDeleted = itemIndex;
    this.isModalActive = true;
  }

  confirmDelete() {
    this.orderItemList.splice(this.itemIndexToBeDeleted, 1);
    this.isModalActive = false;
  }

  get subTotal(): number {
    return this.orderItemList.map(item => item.total).reduce((total, value) => total + value, 0);
  }
}
