import { NgModule } from '@angular/core';
import { PlaceOrderComponent } from './place-order/place-order.component';
import {RouterModule} from '@angular/router';
import { AddOrderItemComponent } from './add-order-item/add-order-item.component';
import { ListOrderItemsComponent } from './list-order-items/list-order-items.component';
import {SharedModule} from '../shared/shared.module';
import {ItemResolverService} from '../core/item-resolver.service';
import { SuccessComponent } from './success/success.component';

const ORDER_ROUTS = [
  {
    path: 'orders/new',
    component: AddOrderItemComponent,
    resolve: { items: ItemResolverService }
  },
  {
    path: 'orders/confirm',
    component: PlaceOrderComponent,
  },
  {
    path: 'orders/success',
    component: SuccessComponent,
  }
]


@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ORDER_ROUTS)
  ],
  declarations: [PlaceOrderComponent, AddOrderItemComponent, ListOrderItemsComponent, SuccessComponent]
})
export class OrdersModule { }
