export interface OrderItem {
  itemId: string;
  name: string;
  unitPrice: number;
  quantity: number;
  total: number;
}
